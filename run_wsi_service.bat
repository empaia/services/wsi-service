@echo off
REM Check if the first argument (WS_DATA_DIR) is provided
IF "%1"=="" (
    echo Error: Please provide the path for WS_DATA_DIR.
    exit /b 1
)

REM Set the environment variables
SET WEB_CONCURRENCY=8
SET WS_CORS_ALLOW_ORIGINS=["*"]
SET WS_DATA_DIR=%1

REM Run the Python server with the specified environment variables
python -m uvicorn wsi_service.app:app --host 0.0.0.0 --port 8080 --loop asyncio --http=httptools
