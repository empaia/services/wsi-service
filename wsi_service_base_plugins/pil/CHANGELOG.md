# Changelog

## 0.7.0

- Switched to poetry v2
- Updated dependencies

## 0.6.1

- Updated dependencies

## 0.6.0

- Updated dependencies

## 0.5.1

- Updated dependencies

## 0.5.0

- Dropped Python 3.9 support

## 0.4.3

- Removed python version limit

## 0.4.2

- Updated dependencies

## 0.4.1

- Updated dependencies

## 0.4.0

- Updated dependencies

## 0.3.1

- Updated dependencies

## 0.3.0

- Removed parameter validation (already part of wsi service)
- Changed default tile size to 256 x 256

## 0.2.0

- Updated to v3 models

## 0.1.1

- Updated dependencies
