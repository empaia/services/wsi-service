# Changelog

## 0.5.3

- Updated dependencies
- Unpinned tifffile due to tiffslide update

## 0.5.2

- Pinned tifffile to 2025.1.10

## 0.5.1

- Pinned zarr<3

## 0.5.0

- Switched to poetry v2
- Updated dependencies

## 0.4.1

- Updated Dependencies

## 0.4.0

- Updated Dependencies

## 0.3.1

- Updated Dependencies

## 0.3.0

- Dropped Python 3.9 support

## 0.2.6

- Updated Dependencies

## 0.2.5

- Removed python version limit

## 0.2.4

- Updated Dependencies

## 0.2.3

- Updated Dependencies

## 0.2.2

- Updated Dependencies

## 0.2.1

- Missing huffman tables fix

## 0.2.0

- Updated for new plugin concept

## 0.1.3

- Updated Dependencies

## 0.1.2

- Fixed direct tile access for YCbCr

## 0.1.1

- tiffslide and openslide compatibility fix

## 0.1.0

- Init
