# Changelog

## 1.7.0

- Switched to poetry v2
- Updated dependencies

## 1.6.1

- Updated dependencies

## 1.6.0

- Removed custom openslide
- Added openslide-bin

## 1.5.1

- Updated dependencies

## 1.5.0

- Dropped Python 3.9 support

## 1.4.5

- Updated dependencies

## 1.4.4

- Removed python version limit

## 1.4.3

- Updated dependencies

## 1.4.2

- Updated dependencies

## 1.4.1

- Updated dependencies

## 1.4.0

- Updated for new plugin concept

## 1.3.1

- Updated dependencies

## 1.3.0

- Removed parameter validation (already part of wsi service)
- Removed special handling for vsf

## 1.2.0

- Updated to v3 models

## 1.1.1

- Updated dependencies

## 1.1.0

- Create and cache thumbnail on intial slide creation
