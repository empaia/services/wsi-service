# Changelog

## 0.7.2

- Updated dependencies

## 0.7.1

- Updated dependencies

## 0.7.0

- Switched to poetry v2
- Updated dependencies

## 0.6.1

- Updated dependencies

## 0.6.0

- Updated dependencies

## 0.5.1

- Updated dependencies

## 0.5.0

- Dropped Python 3.9 support

## 0.4.5

- Removed python version limit

## 0.4.4

- Updated dependencies

## 0.4.3

- Updated dependencies

## 0.4.0

- Updated for new plugin concept

## 0.3.1

- Updated wsidicom

## 0.3.0

- Removed parameter validation (already part of wsi service)
- Removed special handling for out of image regions

## 0.2.0

- Updated to v3 models

## 0.1.0

- Added wsidocom based plugin
