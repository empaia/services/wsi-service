# Changelog

## 0.7.2

- Updated dependencies
- Unpinned tifffile due to tiffslide update

## 0.7.1

- Pinned tifffile to 2025.1.10

## 0.7.0

- Switched to poetry v2
- Updated dependencies

## 0.6.1

- Updated dependencies

## 0.6.0

- Added support for brightfield images
- Updated dependencies

## 0.5.1

- Updated dependencies

## 0.5.0

- Dropped Python 3.9 support

## 0.4.7

- Updated dependencies

## 0.4.6

- Pinned scikit-image version to <0.23.0

## 0.4.5

- Removed python version limit

## 0.4.4

- Updated dependencies

## 0.4.2

- Updated dependencies

## 0.4.1

- Updated dependencies

## 0.4.0

- Updated for new plugin concept

## 0.3.1

- Updated dependencies

## 0.3.0

- Removed parameter validation (already part of wsi service)

## 0.2.0

- Updated to v3 models

## 0.1.3

- Updated dependencies

## 0.1.2

- Updated dependencies

## 0.1.1

- Updated dependencies
