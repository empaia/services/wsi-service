#!/bin/bash

# Check if WS_DATA_DIR is provided as a parameter
if [ -z "$1" ]; then
    echo "Error: Please provide the path for WS_DATA_DIR."
    exit 1
fi

# Set the environment variables
WEB_CONCURRENCY=8
WS_CORS_ALLOW_ORIGINS='["*"]'
WS_DATA_DIR=$1

# Run the Python server with the specified environment variables
python3 -m uvicorn wsi_service.app:app --host 0.0.0.0 --port 8080 --loop=uvloop --http=httptools
